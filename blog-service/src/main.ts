import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');

  //configure validation pipeline
  app.useGlobalPipes(new ValidationPipe());

  //API Documentation.
  const config = new DocumentBuilder()
    .setTitle('Blog API Documentation')
    .setDescription('The Blog API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  //Manage CORS
  app.enableCors();

  const PORT = process.env.PORT || 3000;
  await app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
  });
}
bootstrap();
